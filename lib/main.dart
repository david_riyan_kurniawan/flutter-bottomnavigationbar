import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //membuat variable index yang bertipe data int
  late int index;

/*membuat list index, yang nantinya akan di tampilkan pada halaman
 ketika kita klik menu pada bottom navigation bar nya*/
  List showWidget = [
    Center(
      child: Text('home'),
    ),
    Center(
      child: Text('person'),
    )
  ];

  @override
  //membuat function untuk menampung variable index
  void initState() {
    // TODO: implement initState
    index = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bottom Navigation Bar'),
        centerTitle: true,
      ),
      body: showWidget[index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index, // memasukkan index yang telah kita buat
        backgroundColor: Colors.blue,
        showSelectedLabels: true,
        selectedItemColor: Colors.white,
        onTap: (value) {
          //membuat setState, karna kita hendak melakukan perubahan pada tampilan
          setState(() {
            index = value;
          });
        },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Person'),
        ],
      ),
    );
  }
}
